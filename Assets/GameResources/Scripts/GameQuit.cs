﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Выход из игры
/// </summary>
public class GameQuit : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
