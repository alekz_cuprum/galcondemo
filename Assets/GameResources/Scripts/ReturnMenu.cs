﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/// <summary>
/// Возвращение в меню
/// </summary>
public class ReturnMenu : MonoBehaviour
{
    private const string MENU_NAME = "Menu";

    private void Update()
    {
        if(Input.GetKeyUp(KeyCode.Escape))
        {
            SceneManager.LoadScene(MENU_NAME);
        }
    }
}
