﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Управление планетами игрока
/// </summary>
public class PlayerController : MonoBehaviour
{
    private int numberControlPlanets;
    private int maxControlPlanets;

    /// <summary>
    /// Событие проигрыша
    /// </summary>
    public System.Action OnLoseGame = delegate { };

    /// <summary>
    /// Событие победы
    /// </summary>
    public System.Action OnWinGame = delegate { };

    private void Start()
    {
        numberControlPlanets = 1;
    }

    /// <summary>
    /// Уменьшение контролируемых планет
    /// </summary>
    public void MinusControlPlanet()
    {
        numberControlPlanets--;
        ControlNumberPlanets();
    }

    /// <summary>
    /// Увеличение контролируемых планет
    /// </summary>
    public void PlusControlPlanet()
    {
        numberControlPlanets++;
        ControlNumberPlanets();
    }

    /// <summary>
    /// Изменение максимального числа кол-ва планет, которым можно обладать для победы
    /// </summary>
    public void ChangeMaxControlPlanets(int newMaximum)
    {
        maxControlPlanets = newMaximum;
    }

    private void ControlNumberPlanets()
    {
        if (numberControlPlanets == 0)
        {
            OnLoseGame.Invoke();
        }
        else if (numberControlPlanets == maxControlPlanets)
        {
            OnWinGame.Invoke();
        }
    }

}
