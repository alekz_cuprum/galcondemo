﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Боевой космический корабль
/// </summary>
[RequireComponent(typeof(NavMeshAgent))]
public class Spaceship : MonoBehaviour
{
    private BelongingPlanet belonging;

    private NavMeshAgent agent;
    private Material material;
    private Collider goal;
    private Planet attackedPlanet;

    private const string NAME_COLOR = "_Color";

    /// <summary>
    /// Событие столкновения с атакуемой планетой
    /// </summary>
    public System.Action<Planet, BelongingPlanet> OnCollisionAttackedPlanet = delegate { };

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        belonging = BelongingPlanet.Nobody;

        Renderer renderer = GetComponent<Renderer>();
        material = renderer.material;
    }

    /// <summary>
    /// Изменение принадлежности корабля
    /// </summary>
    public void ChangeBelonging(BelongingPlanet newBelonging, Color32 color)
    {
        belonging = newBelonging;
        material.SetColor(NAME_COLOR, color);
    }

    /// <summary>
    /// Атака выбранной планеты
    /// </summary>
    public void Attack(Planet newAttackedPlanet, Transform transformPlanet, Collider newGoal)
    {
        agent.destination = transformPlanet.position;
        goal = newGoal;
        attackedPlanet = newAttackedPlanet;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider == goal)
        {
            OnCollisionAttackedPlanet.Invoke(attackedPlanet, belonging);
            Destroy(gameObject);
        }
    }
}
