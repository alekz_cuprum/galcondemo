﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewStatusSelectPlanet : MonoBehaviour
{
    [SerializeField] private Planet planet;
    [SerializeField] private GameObject elementOfSelect;

    private void Start()
    {
        planet.OnChangeStatusSelect += ChangeElementSelect;
    }

    private void ChangeElementSelect(bool status)
    {
        elementOfSelect.SetActive(status);
    }

    private void OnDestroy()
    {
        planet.OnChangeStatusSelect -= ChangeElementSelect;
    }
}
