﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Класс для выделения планет игрока
/// </summary>
public class SelectPlanetController : MonoBehaviour
{
    private RaycastHit hit;
    private Ray ray;

    private Planet selectPlanet;

    private bool isSelectPlanet;
    /// <summary>
    /// Событие отмены выделения планет
    /// </summary>
    public System.Action OnCancelSelect = delegate { };

    /// <summary>
    /// Событие атаки планеты
    /// </summary>
    public System.Action<GameObject, Collider, Planet> OnAttackPlanet = delegate { };

    private void Start()
    {
        selectPlanet = null;
        isSelectPlanet = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Select();
        }
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            isSelectPlanet = false;
            OnCancelSelect.Invoke();
        }
    }

    private void Select()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            selectPlanet = hit.collider.gameObject.GetComponent<Planet>();

            if (selectPlanet != null)
            {
                if (selectPlanet.BelongingPlanet == BelongingPlanet.Player)
                {
                    selectPlanet.ChangeSelectPlanet(true);
                    isSelectPlanet = true;
                }
                else if (isSelectPlanet)
                {
                    OnAttackPlanet.Invoke(selectPlanet.gameObject, hit.collider, selectPlanet);
                    isSelectPlanet = false;
                }
            }
        }
    }

}
