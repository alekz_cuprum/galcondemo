﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Создатель атакующих кораблей
/// </summary>
public class CreatorSpaceship : MonoBehaviour
{
    [SerializeField] private Object prefabShip;
    [SerializeField] private GameObject spawn;

    [SerializeField] private float radiusCreate;

    /// <summary>
    /// Создать корабли около указанной планеты
    /// </summary>
    public List<Spaceship> Create(Planet attackedPlanet, Collider attackedCollider, Transform transformPlanet, Transform transformAttackedPlanet, int numberShip, BelongingPlanet belonging, Color32 color)
    {
        int i;
        float startPositionX, startPositionZ;
        float radiusPlanet = transformPlanet.localScale.x;
        float radiusRandomCreate = radiusPlanet + radiusCreate;
        List<Spaceship> spaceships = new List<Spaceship>();

        for (i = 0; i < numberShip; i++)
        {
            GameObject ship = Instantiate(prefabShip, spawn.transform) as GameObject;

                startPositionX = Random.Range(-radiusRandomCreate, radiusRandomCreate);
                startPositionZ = Random.Range(-radiusRandomCreate, radiusRandomCreate);

            ship.transform.position = new Vector3(transformPlanet.position.x + startPositionX, 0f, transformPlanet.position.z + startPositionZ);
            
            Spaceship spaceship = ship.GetComponent<Spaceship>();
            spaceship.ChangeBelonging(belonging, color);
            spaceship.Attack(attackedPlanet, transformAttackedPlanet, attackedCollider);
            
            spaceships.Add(spaceship);
        }

        return spaceships;
    }
}
