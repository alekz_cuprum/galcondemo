﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Создание планет на карте
/// </summary>
public class SpawnerPlanet : MonoBehaviour
{
    [Header("Шаблон планеты")]
    [SerializeField] private Object prefabPlanet;

    [Header("Место спавна")]
    [SerializeField] private GameObject spawn;

    [Header("Границы спавна по координатам")]
    [SerializeField] private float minX = -175f;
    [SerializeField] private float maxX = 175f;
    [SerializeField] private float minZ = -85f;
    [SerializeField] private float maxZ = 85f;


    private float minScalePlanet;
    private float maxScalePlanet;

    private int minRandomNumberPlanets;
    private int maxRandomNumberPlanets;

    private int numberPlanets;

    public System.Action<List<GameObject>> OnSpawn = delegate { };

    private List<GameObject> listPlanet = new List<GameObject>();

    /// <summary>
    /// Спавн планет
    /// </summary>
    public void Spawn(DifficultyGame difficulty)
    {
        LoadSettings(difficulty);
        StartSpawn();
    }

    private void LoadSettings(DifficultyGame difficulty)
    {
        switch(difficulty)
        {
            case DifficultyGame.easy:
                minScalePlanet = 25f;
                maxScalePlanet = 35f;
                minRandomNumberPlanets = 7;
                maxRandomNumberPlanets = 12;
                break;
            case DifficultyGame.medium:
                minScalePlanet = 20f;
                maxScalePlanet = 30f;
                minRandomNumberPlanets = 11;
                maxRandomNumberPlanets = 16;
                break;
            case DifficultyGame.hard:
                minScalePlanet = 15f;
                maxScalePlanet = 25f;
                minRandomNumberPlanets = 15;
                maxRandomNumberPlanets = 23;
                break;
        }
    }

    private void StartSpawn()
    {
        int i;
        float scalePlanet;
        bool isCorrectPosition;
        float distance, minDistance;
        numberPlanets = Random.Range(minRandomNumberPlanets,maxRandomNumberPlanets);

        for(i = 0; i < numberPlanets; i++)
        {
            GameObject planet = Instantiate(prefabPlanet, spawn.transform) as GameObject;
            isCorrectPosition = false;

            while (!isCorrectPosition)
            {
                planet.transform.position = new Vector3(Random.Range(minX, maxX), 0f, Random.Range(minZ, maxZ));
                scalePlanet = Random.Range(minScalePlanet, maxScalePlanet);
                planet.transform.localScale = new Vector3(scalePlanet, scalePlanet, scalePlanet);

                if (i != 0)
                {
                    foreach (GameObject obj in listPlanet)
                    {
                        distance = Mathf.Sqrt(Mathf.Pow((planet.transform.position.x - obj.transform.position.x),2) + Mathf.Pow((planet.transform.position.z - obj.transform.position.z),2));
                        minDistance = planet.transform.localScale.x + obj.transform.localScale.x;

                        isCorrectPosition = distance > minDistance;
                        if (!isCorrectPosition) 
                            break;
                    }
                }
                else
                {
                    isCorrectPosition = true;
                }
            }
            listPlanet.Add(planet);
        }

        OnSpawn.Invoke(listPlanet);
    }
}
