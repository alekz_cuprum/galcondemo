﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Главный котроллер игровой сцены
/// </summary>
public class GameController : MonoBehaviour
{
    [Header("Файл с настройками игры")]
    [SerializeField] private GameSettings gameSettings;

    [Header("Котроллеры игровой сцены")]
    [SerializeField] private SpawnerPlanet spawnerPlanet;
    [SerializeField] private PlayerController playerController;
    [SerializeField] private SelectPlanetController selectPlanetController;
    [SerializeField] private CreatorSpaceship creatorSpaceship;
    [SerializeField] private EndGameController endGameController;

    private List<Planet> listPlanets = new List<Planet>();

    private void Start()
    {
        spawnerPlanet.OnSpawn += FinishSpawnPlanets;
        playerController.OnWinGame += WinGame;
        playerController.OnLoseGame += LoseGame;
        selectPlanetController.OnCancelSelect += CancelSelectPlanets;
        selectPlanetController.OnAttackPlanet += AttackPlanet;

        spawnerPlanet.Spawn(gameSettings.Difficulty);
    }

    private void OnDestroy()
    {
        spawnerPlanet.OnSpawn -= FinishSpawnPlanets;
        playerController.OnWinGame -= WinGame;
        playerController.OnLoseGame -= LoseGame;
        selectPlanetController.OnCancelSelect -= CancelSelectPlanets;
        selectPlanetController.OnAttackPlanet -= AttackPlanet;
    }

    private void LoseGame()
    {
        endGameController.EndGame(false);
    }

    private void WinGame()
    {
        endGameController.EndGame(true);
    }

    private void FinishSpawnPlanets(List<GameObject> list)
    {
        foreach (GameObject obj in list)
        {
            listPlanets.Add(obj.GetComponent<Planet>());
        }

        foreach (Planet planet in listPlanets)
        {
            planet.OnChangeBelonging += ChangeNumberControlledPlanets;
        }
        playerController.ChangeMaxControlPlanets(listPlanets.Count);

        SelectFirstPlanet();
    }

    private void ChangeNumberControlledPlanets(BelongingPlanet belonging, bool status)
    {
        if ((belonging == BelongingPlanet.Player) && (!status))
        {
            playerController.PlusControlPlanet();
        }
        if ((belonging != BelongingPlanet.Player) && status)
        {
            playerController.MinusControlPlanet();
        }
    }

    private void SelectFirstPlanet()
    {
        switch(gameSettings.NumberOfOpponents)
        {
            case 3:
                RandomSelect(BelongingPlanet.Enemy3, gameSettings.EnemyColor3);
                goto case 2;
            case 2:
                RandomSelect(BelongingPlanet.Enemy2, gameSettings.EnemyColor2);
                goto case 1;
            case 1:
                RandomSelect(BelongingPlanet.Enemy1, gameSettings.EnemyColor1);
                goto case 0;
            case 0:
                RandomSelect(BelongingPlanet.Player, gameSettings.PlayerColor, gameSettings.StartHealthPlayer);
                break;
        }
    }    

    private void RandomSelect(BelongingPlanet belonging, Color32 color)
    {
        int numberRandomPlanet;

        while (true)
        {
            numberRandomPlanet = Random.Range(0, listPlanets.Count - 1);
            if (listPlanets[numberRandomPlanet].BelongingPlanet == BelongingPlanet.Nobody)
                break;
        }
        listPlanets[numberRandomPlanet].ChangeBelonging(belonging, color);
    }

    private void RandomSelect(BelongingPlanet belonging, Color32 color, int startHealth)
    {
        int numberRandomPlanet;

        while (true)
        {
            numberRandomPlanet = Random.Range(0, listPlanets.Count - 1);
            if (listPlanets[numberRandomPlanet].BelongingPlanet == BelongingPlanet.Nobody)
                break;
        }
        listPlanets[numberRandomPlanet].ChangeBelonging(belonging, color, startHealth);
    }

    private void CancelSelectPlanets()
    {
        foreach (Planet planet in listPlanets)
        {
            planet.ChangeSelectPlanet(false);
        }
    }

    private void AttackPlanet(GameObject objPlanet, Collider attackedCollider, Planet attackedPlanet)
    {
        int numberCreateShip;
        List<Spaceship> spaceships = new List<Spaceship>();

        foreach(Planet planet in listPlanets)
        {
            if (planet.IsSelect)
            {
                numberCreateShip = planet.AttackAnotherPlanet();

                spaceships = creatorSpaceship.Create(attackedPlanet, attackedCollider, planet.gameObject.transform, objPlanet.transform, numberCreateShip, BelongingPlanet.Player, gameSettings.PlayerColor);
                planet.ChangeSelectPlanet(false);

                foreach (Spaceship ship in spaceships)
                {
                    ship.OnCollisionAttackedPlanet += DamagingPlanet;
                }
            }
            
        }
    }

    /// <summary>
    /// нанесение урона планете
    /// </summary>
    private void DamagingPlanet(Planet planet, BelongingPlanet belongingShip)
    {
        Color32 color = gameSettings.PlayerColor;
        switch(belongingShip)
        {
            case BelongingPlanet.Player:
                color = gameSettings.PlayerColor;
                break;
            case BelongingPlanet.Enemy1:
                color = gameSettings.EnemyColor1;
                break;
            case BelongingPlanet.Enemy2:
                color = gameSettings.EnemyColor2;
                break;
            case BelongingPlanet.Enemy3:
                color = gameSettings.EnemyColor3;
                break;
        }

        planet.AttackThisPlanet(belongingShip, color);
    }    

    
}
