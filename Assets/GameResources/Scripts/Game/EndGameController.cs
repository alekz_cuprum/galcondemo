﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
/// <summary>
/// Контроллер окончания игры
/// </summary>
public class EndGameController : MonoBehaviour
{
    [SerializeField] private Canvas canvas;
    [SerializeField] private Text text;
    [SerializeField] private Button button;

    private const string TEXT_WIN = "Win";
    private const string TEXT_LOSE = "Lose";
    private const string NAME_SCENE_MENU = "Menu";
    /// <summary>
    /// Окончание игры
    /// </summary>
    public void EndGame(bool isWin)
    {
        canvas.gameObject.SetActive(true);
        text.text = isWin ? TEXT_WIN : TEXT_LOSE;

        button.onClick.AddListener(ReturnMenu);
        Time.timeScale = 0f;
    }

    private void ReturnMenu()
    {
        SceneManager.LoadScene(NAME_SCENE_MENU);
    }
}
