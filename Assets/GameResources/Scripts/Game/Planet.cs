﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Принадлежность планеты
/// </summary>
public enum BelongingPlanet
{
    Nobody = 0,
    Player,
    Enemy1,
    Enemy2,
    Enemy3
}

/// <summary>
/// Основной скрипт планеты
/// </summary>
public class Planet : MonoBehaviour
{
    [SerializeField] private int minRandomHealth = 10;
    [SerializeField] private int maxRandomHealth = 40;

    [Header("Параметры восстановления жизни")]
    [SerializeField] private float delayRecoveryHealth = 1f;
    [SerializeField] private int recoveryHealth = 5;

    private int health;
    public int Health => health;

    private bool isSelect = false;
    public bool IsSelect => isSelect;

    private Coroutine recoveryHealthCoroutine = null;

    private const string NAME_COLOR = "_Color";

    /// <summary>
    /// Событие изменения жизни
    /// </summary>
    public System.Action<int> OnChangeHealth = delegate { };

    /// <summary>
    /// Событие изменения статуса выделения планеты
    /// </summary>
    public System.Action<bool> OnChangeStatusSelect = delegate { };

    /// <summary>
    /// Событие изменение статуса планеты
    /// </summary>
    public System.Action<BelongingPlanet, bool> OnChangeBelonging = delegate { };
    /// <summary>
    /// Принадлежность планеты
    /// </summary>
    public BelongingPlanet BelongingPlanet => belongingPlanet;
    private BelongingPlanet belongingPlanet;

    private Material material;

    private void Awake()
    {
        health = Random.Range(minRandomHealth, maxRandomHealth);
        OnChangeHealth.Invoke(health);

        belongingPlanet = BelongingPlanet.Nobody;
        Renderer renderer = GetComponent<Renderer>();
        material = renderer.material;

        ChangeSelectPlanet(false);
    }

    /// <summary>
    /// Изменение принадлежности планеты
    /// </summary>
    public void ChangeBelonging(BelongingPlanet newBelonging, Color32 newColor)
    {
        OnChangeBelonging.Invoke(newBelonging, (belongingPlanet == BelongingPlanet.Player));
        belongingPlanet = newBelonging;

        material.SetColor(NAME_COLOR, newColor);

        if (belongingPlanet != BelongingPlanet.Nobody)
        {
            recoveryHealthCoroutine = StartCoroutine(RecoveryHealth());
        }


    }

    /// <summary>
    /// Изменение принадлежности планеты с определенными начальными жизнями
    /// </summary>
    public void ChangeBelonging(BelongingPlanet newBelonging, Color32 newColor, int startHealth)
    {
        belongingPlanet = newBelonging;

        material.SetColor(NAME_COLOR, newColor);

        health = startHealth;

        if (belongingPlanet != BelongingPlanet.Nobody)
        {
            recoveryHealthCoroutine = StartCoroutine(RecoveryHealth());
        }
    }

    private IEnumerator RecoveryHealth()
    {
        while (true)
        {
            yield return new WaitForSeconds(delayRecoveryHealth);
            
            health += 5;
            OnChangeHealth.Invoke(health);
        }
    }

    /// <summary>
    /// Атака другой планеты
    /// </summary>
    public int AttackAnotherPlanet()
    {
        health /= 2;
        OnChangeHealth.Invoke(health);
        int numberCreateShip = (health % 2 == 0) ? health : (health + 1);
        return numberCreateShip;
    }

    /// <summary>
    /// Эту планету атакуют
    /// </summary>
    public void AttackThisPlanet(BelongingPlanet belongingAttackShip, Color32 color)
    {
        if (belongingPlanet != belongingAttackShip)
        {
            health--;

            if (health < 0)
            {
                ChangeBelonging(belongingAttackShip, color);
            }
        }
        else
        {
            health++;
        }
        OnChangeHealth.Invoke(health);
    }


    /// <summary>
    /// Изменения статуса выбора планеты
    /// </summary>
    public void ChangeSelectPlanet(bool status)
    {
        isSelect = status;
        OnChangeStatusSelect.Invoke(isSelect);
    }

    private void OnDestroy()
    {
        if (recoveryHealthCoroutine != null)
        {
            StopCoroutine(recoveryHealthCoroutine);
            recoveryHealthCoroutine = null;
        }    
    }
}
