﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Кнопка выхода
/// </summary>
public class ButtonQuit : UIButton
{
    public override void ActionOnClick()
    {
        Application.Quit();
    }

}
