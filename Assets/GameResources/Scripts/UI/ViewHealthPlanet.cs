﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Отображение жизни планеты на канвасе
/// </summary>
public class ViewHealthPlanet : MonoBehaviour
{
    [SerializeField] private Planet planet;
    [SerializeField] private Canvas canvasPlanet;
    [SerializeField] private Text textHealth;

    private void Start()
    {
        canvasPlanet.worldCamera = FindObjectOfType<Camera>();
        planet.OnChangeHealth += UpdateHealth;
        UpdateHealth(planet.Health);
    }

    private void UpdateHealth(int health)
    {
        textHealth.text = health.ToString();
    }

    private void OnDestroy()
    {
        planet.OnChangeHealth -= UpdateHealth;
    }
}
