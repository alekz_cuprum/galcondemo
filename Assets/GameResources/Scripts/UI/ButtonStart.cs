﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/// <summary>
/// Кнопка старта игры
/// </summary>
public class ButtonStart : UIButton
{
    private const string NAME_GAME_SCENE = "Game";

    public override void ActionOnClick()
    {
        SceneManager.LoadScene(NAME_GAME_SCENE);
    }
}
