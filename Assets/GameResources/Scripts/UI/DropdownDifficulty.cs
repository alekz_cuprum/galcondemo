﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Dropdown, который управляет сложностью игры
/// </summary>
[RequireComponent(typeof(Dropdown))]
public class DropdownDifficulty : MonoBehaviour
{
    [SerializeField] private GameSettings gameSettings;
    private Dropdown dropdown;

    private void Awake()
    {
        dropdown = GetComponent<Dropdown>();
        dropdown.onValueChanged.AddListener(ChangeValue);
    }

    private void ChangeValue(int status)
    {
        switch(status)
        {
            case 0:
                gameSettings.SaveSettings(DifficultyGame.easy);
                break;
            case 1:
                gameSettings.SaveSettings(DifficultyGame.medium);
                break;
            case 2:
                gameSettings.SaveSettings(DifficultyGame.hard);
                break;
        }    
    }

}
