﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Базовый класс кнопки интерфейса
/// </summary>
[RequireComponent(typeof(Button))]
public abstract class UIButton : MonoBehaviour
{
    protected Button button;

    protected void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(ActionOnClick);
    }

    /// <summary>
    /// Действие на нажатие кнопки
    /// </summary>
    public abstract void ActionOnClick();
}
