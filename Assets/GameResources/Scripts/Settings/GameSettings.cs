﻿using UnityEngine;

/// <summary>
/// Сложность игры
/// </summary>
public enum DifficultyGame
{
    easy = 0,
    medium,
    hard
}

/// <summary>
/// Объект с настройками игры
/// </summary>
[CreateAssetMenu(fileName ="GameSettings", menuName ="ScriptableObjects/Game Settings", order = 51)]
public class GameSettings : ScriptableObject
{
    [SerializeField, Range(1,99)] private int startHealthPlayer = 50;
    [SerializeField] private DifficultyGame difficulty = DifficultyGame.easy;
    [SerializeField, Range(0,3)] private int numberOfOpponents = 0;
    
    [Header("Цвета игроков")]
    [SerializeField] private Color32 playerColor = new Color32(0, 0, 255, 255);
    [SerializeField] private Color32 enemyColor1 = new Color32(255, 0, 0, 255);
    [SerializeField] private Color32 enemyColor2 = new Color32(0, 255, 0, 255);
    [SerializeField] private Color32 enemyColor3 = new Color32(255, 255, 0, 255);

    /// <summary>
    /// Стартовая жизнь игрока
    /// </summary>
    public int StartHealthPlayer => startHealthPlayer;

    /// <summary>
    /// Игровая сложность
    /// </summary>
    public DifficultyGame Difficulty => difficulty;

    /// <summary>
    /// Кол-во противников
    /// </summary>
    public int NumberOfOpponents => numberOfOpponents;

    /// <summary>
    /// Цвет игрока
    /// </summary>
    public Color32 PlayerColor => playerColor;

    public Color32 EnemyColor1 => enemyColor1;
    public Color32 EnemyColor2 => enemyColor2;
    public Color32 EnemyColor3 => enemyColor3;

    /// <summary>
    /// Сохранение настроек игры
    /// </summary>
    public void SaveSettings(DifficultyGame newDifficulty, int newNumberOpponents, Color32 newPlayerColor)
    {
        difficulty = newDifficulty;
        playerColor = newPlayerColor;

        if ((newNumberOpponents >= 0) && (newNumberOpponents <=3))
            numberOfOpponents = newNumberOpponents;
    }

    /// <summary>
    /// Сохранение настроек игры
    /// </summary>
    public void SaveSettings(DifficultyGame newDifficulty, int newNumberOpponents)
    {
        difficulty = newDifficulty;

        if ((newNumberOpponents >= 0) && (newNumberOpponents <= 3))
            numberOfOpponents = newNumberOpponents;
    }

    /// <summary>
    /// Сохранение настроек игры
    /// </summary>
    public void SaveSettings(DifficultyGame newDifficulty)
    {
        difficulty = newDifficulty;
    }
}
